# README #

JSON API for front end(Mobile and Website) witch combile with 2 features.


### 1. Installation ###

* Install MongoDB
* Install Node.js
* Edit file index.js in src/config/ for change port and database setting

![edit mongodb.png](https://bitbucket.org/repo/LyMoxA/images/4206007211-edit%20mongodb.png)


* npm install
* npm run dev (For develop and testing)
* npm run build (For production on server)


* Set header for Postman
![set header.png](https://bitbucket.org/repo/LyMoxA/images/560778516-set%20header.png)



### 2. Authentication  ###

* User can sign up with email and password.  
* post url : localhost:8888/api/v1/user/register

![user register.png](https://bitbucket.org/repo/LyMoxA/images/1742583396-user%20register.png)

* User can login with email and password. 
* post url : localhost:8888/api/v1/user/login

![user login.png](https://bitbucket.org/repo/LyMoxA/images/2486327687-user%20login.png)

* After login or sign up succeed, user can use the token(ex. JWT token) to manage the products in the system.

* For to set Authorization for Route you can use 'authenticate' into method.

* Ex.  api.get('/me', **authenticate**, (req, res) => { ...

* And set token to header in postman when you test.

![set header authori.png](https://bitbucket.org/repo/LyMoxA/images/1562191851-set%20header%20authori.png)


### 3. CRUD Products  ###

* User can get list of products.  
* get url : localhost:8888/api/v1/product 

![get all product.png](https://bitbucket.org/repo/LyMoxA/images/360602458-get%20all%20product.png)

* User can get a product with more detail.
* get url : localhost:8888/api/v1/product/'Product ID'

![get product by id.png](https://bitbucket.org/repo/LyMoxA/images/1569546635-get%20product%20by%20id.png)

* User can create product.
* post url : localhost:8888/api/v1/product/add

![add product.png](https://bitbucket.org/repo/LyMoxA/images/3562855804-add%20product.png)

* User can update product. (Bonus: PATCH Method)
* put url : localhost:8888/api/v1/product/'Product ID'

![put product.png](https://bitbucket.org/repo/LyMoxA/images/239854699-put%20product.png)

* patch url : localhost:8888/api/v1/product/'Product ID'

![patch product.png](https://bitbucket.org/repo/LyMoxA/images/170505765-patch%20product.png)

* User can delete product or list of products.
* delete url : localhost:8888/api/v1/product/'Product ID'

![delete product.png](https://bitbucket.org/repo/LyMoxA/images/25151089-delete%20product.png)

Bonus: Product have image

* newproduct url : localhost:8888/newproduct

![newproduct copy.jpg](https://bitbucket.org/repo/LyMoxA/images/2801908601-newproduct%20copy.jpg)


### 4. Testing ###
* Coming Soon