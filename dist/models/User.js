'use strict';

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _passportLocalMongoose = require('passport-local-mongoose');

var _passportLocalMongoose2 = _interopRequireDefault(_passportLocalMongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var UserSchema = new Schema({

    title: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: String,
    password: String
    /*email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }*/

});

UserSchema.plugin(_passportLocalMongoose2.default);
module.exports = _mongoose2.default.model('User', UserSchema);
//# sourceMappingURL=user.js.map