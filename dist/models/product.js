'use strict';

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var ProductSchema = new Schema({

    name: {
        type: String,
        required: true
    },
    detail: {
        type: String
    },
    price: Number,
    image_url: String

});

module.exports = _mongoose2.default.model('Product', ProductSchema);
//# sourceMappingURL=product.js.map