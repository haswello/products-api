'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

var _middleware = require('../middleware');

var _middleware2 = _interopRequireDefault(_middleware);

var _db = require('../db');

var _db2 = _interopRequireDefault(_db);

var _product = require('../controllers/product');

var _product2 = _interopRequireDefault(_product);

var _user = require('../controllers/user');

var _user2 = _interopRequireDefault(_user);

var _upload = require('../controllers/upload');

var _upload2 = _interopRequireDefault(_upload);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = (0, _express2.default)();

// connect to db
(0, _db2.default)(function (db) {

    // internal middleware
    router.use((0, _middleware2.default)({ config: _config2.default, db: db }));

    // api route v1 (/v1)
    router.use('/product', (0, _product2.default)({ config: _config2.default, db: db }));
    router.use('/user', (0, _user2.default)({ config: _config2.default, db: db }));

    router.use('/upload', (0, _upload2.default)({ config: _config2.default, db: db }));
});

exports.default = router;
//# sourceMappingURL=index.js.map