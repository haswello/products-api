'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _express = require('express');

var _Product = require('../models/Product');

var _Product2 = _interopRequireDefault(_Product);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var config = _ref.config,
        db = _ref.db;


    var api = (0, _express.Router)();

    // CRUD - Create Read Update Delete
    // '/v1/product/add' - Create
    api.post('/add', function (req, res) {

        var newProduct = new _Product2.default();
        newProduct.name = req.body.name;
        newProduct.detail = req.body.detail;
        newProduct.price = req.body.price;
        newProduct.image_url = req.body.image_url;

        newProduct.save(function (err) {
            if (err) {
                res.send(err);
            }
            res.json({ message: 'Product saved successfully' });
        });
    });

    // '/v1/product' - list of products
    api.get('/', function (req, res) {

        _Product2.default.find({}, function (err, product) {
            if (err) {
                res.send(err);
            }
            res.json(product);
        });
    });

    // '/v1/product/:id' - show product with _id
    api.get('/:id', function (req, res) {

        _Product2.default.findById(req.params.id, function (err, product) {
            if (err) {
                res.send(err);
            }
            res.json(product);
        });
    });

    // '/v1/product/:id' - update product with _id
    api.put('/:id', function (req, res) {

        _Product2.default.findById(req.params.id, function (err, product) {
            if (err) {
                res.send(err);
            }

            product.name = req.body.name;
            product.detail = req.body.detail;
            product.price = req.body.price;
            product.image_url = req.body.image_url;

            product.save(function (err) {
                if (err) {
                    res.send(err);
                }
                res.json({ message: "Product info updated" });
            });
        });
    });

    // '/v1/product/:id' - update product with _id
    api.patch('/:id', function (req, res) {

        _Product2.default.findById(req.params.id, function (err, product) {
            if (err) {
                res.send(err);
            }

            product.name = req.body.name;
            product.detail = req.body.detail;
            product.price = req.body.price;
            product.image_url = req.body.image_url;

            product.save(function (err) {
                if (err) {
                    res.send(err);
                }
                res.json({ message: "Product info updated" });
            });
        });
    });

    // '/v1/product/:id' - delete product with _id
    api.delete('/:id', function (req, res) {

        _Product2.default.remove({ _id: req.params.id }, function (err, product) {
            if (err) {
                res.send(err);
            }
            res.json({ message: "Product successfully removed!" });
        });
    });

    return api;
};
//# sourceMappingURL=product.js.map