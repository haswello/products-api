'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _express = require('express');

var _User = require('../models/User');

var _User2 = _interopRequireDefault(_User);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _authMiddleware = require('../middleware/authMiddleware');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var config = _ref.config,
        db = _ref.db;


    var api = (0, _express.Router)();

    // CRUD - Create Read Update Delete


    // '/v1/photo'
    api.post('/photo', function (req, res) {

        var filename_new = '-';
        var storage = _multer2.default.diskStorage({
            destination: function destination(req, file, callback) {
                callback(null, './uploads');
            },
            filename: function filename(req, file, callback) {
                filename_new = file.fieldname + '-' + Date.now();
                callback(null, filename_new);
            }
        });

        var upload = (0, _multer2.default)({ storage: storage }).array('productPhoto', 2);

        upload(req, res, function (err) {

            if (err) {
                return res.send("Error uploading file.");
            }

            return res.json({ message: "File is uploaded", filename: filename_new });
            //res.send("File is uploaded");
            //res.send(req.files);
            //res.send(req.body);
        });
    });

    // '/v1/photo'
    api.get('/photo/:name', function (req, res) {

        res.sendFile("/Users/kowit/Job/Products-API/uploads/" + req.params.name);
    });

    return api;
};
//# sourceMappingURL=upload.js.map