import mongoose from 'mongoose';

let Schema = mongoose.Schema;

let ProductSchema = new Schema({

    name : {
        type: String,
        required: true
    },
    detail : {
        type: String
    },
    price : Number,
    image_url: String

});

module.exports = mongoose.model('Product', ProductSchema);