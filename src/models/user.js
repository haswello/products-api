import mongoose from 'mongoose';
import passportLocalMongoose from 'passport-local-mongoose';
const Schema = mongoose.Schema;

let UserSchema = new Schema({

    title: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: String,
    password: String
    /*email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }*/

});

UserSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('User', UserSchema);