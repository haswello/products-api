import mongoose from 'mongoose';
import { Router } from 'express';
import User from '../models/User';
import bodyParser from 'body-parser';
import passport from 'passport';
import config from '../config';
import multer from 'multer';

import { generateAccessToken, respond, authenticate } from '../middleware/authMiddleware';

export default({ config, db }) => {

    let api = Router();

    // CRUD - Create Read Update Delete

  
    // '/v1/photo'
    api.post('/photo', (req,res) => {

        let filename_new = '-';
        let storage =   multer.diskStorage({
            destination: (req, file, callback) => {
                callback(null, './uploads');
            },
            filename: (req, file, callback) => {
                filename_new = file.fieldname + '-' + Date.now();
                callback(null, filename_new );
            }
        });

        let upload = multer({ storage : storage }).array('productPhoto',2);

        upload(req,res, (err) => {
            
            if(err) {
               return res.send("Error uploading file.");
            }
          
            return res.json({message : "File is uploaded" , filename : filename_new});
            //res.send("File is uploaded");
            //res.send(req.files);
            //res.send(req.body);
        });
    });

    // '/v1/photo'
    api.get('/photo/:name', (req,res) => {

            res.sendFile("/Users/kowit/Job/Products-API/uploads/"+ req.params.name);
       
    });

    return api;

}