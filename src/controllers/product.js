import mongoose from 'mongoose';
import { Router } from 'express';
import Product from '../models/Product';


export default({ config, db }) => {

    let api = Router();

    // CRUD - Create Read Update Delete
    // '/v1/product/add' - Create
    api.post('/add', (req, res) => {

        
        let newProduct = new Product();
        newProduct.name = req.body.name;
        newProduct.detail = req.body.detail;
        newProduct.price = req.body.price;
        newProduct.image_url = req.body.image_url;

        newProduct.save((err) => {
            if(err){
                res.send(err);
            }
            res.json({message : 'Product saved successfully'});
        });
        
    });


    // '/v1/product' - list of products
    api.get('/', (req ,res) => {

        Product.find({}, (err, product) => {
            if(err){
                res.send(err);
            }
            res.json(product);
        });
    });

    // '/v1/product/:id' - show product with _id
    api.get('/:id', (req, res) => {

        Product.findById(req.params.id, (err, product) => {
            if(err){
                res.send(err);
            }
            res.json(product);
        });
    });

    // '/v1/product/:id' - update product with _id
    api.put('/:id', (req, res) => {

        Product.findById(req.params.id, (err , product) => {
            if(err){
                res.send(err);
            }

            product.name = req.body.name;
            product.detail = req.body.detail;
            product.price = req.body.price;
            product.image_url = req.body.image_url;

            product.save( err => {
                if(err){
                    res.send(err);
                }
                res.json({message : "Product info updated"});
            });
        });
    });

     // '/v1/product/:id' - update product with _id
    api.patch('/:id', (req, res) => {

        Product.findById(req.params.id, (err , product) => {
            if(err){
                res.send(err);
            }

            product.name = req.body.name;
            product.detail = req.body.detail;
            product.price = req.body.price;
            product.image_url = req.body.image_url;

            product.save( err => {
                if(err){
                    res.send(err);
                }
                res.json({message : "Product info updated"});
            });
        });
    });

     // '/v1/product/:id' - delete product with _id
     api.delete('/:id', (req, res) => {

        Product.remove({_id: req.params.id }, (err, product) => {
            if(err){
                res.send(err);
            }
            res.json({message : "Product successfully removed!"});
        });
     });

    return api;

}