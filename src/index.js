import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import passport from 'passport';
import config from './config';
import routes from './routes';

const LocalStrategy = require('passport-local').Strategy;

let app = express();
app.server = http.createServer(app);

// middleware
// parse application/json
app.use(bodyParser.json({
    limit: config.bodyLimit
}));

// passport config
app.use(passport.initialize());
let User = require('./models/User');
passport.use(new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password'
},User.authenticate()
));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


// route test
app.get('/myinfo', (req, res) => {
    res.send({
        name : 'Kowit Subsai',
        email : 'kowit.ks@gmail.com',
        address : 'Thailand'
    });
});

// route test upload 
app.get('/test_upload', (req,res) => {

       res.sendFile(__dirname + "/views/test_upload.html");
});

// route test new product
app.get('/newproduct', (req,res) => {

       res.sendFile(__dirname + "/views/new_product.html");
});




// api routes v1
app.use('/api/v1', routes);

app.server.listen(config.port);
console.log(`Started on port ${app.server.address().port}`);

export default app;


